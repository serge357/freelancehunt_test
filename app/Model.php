<?php

use GuzzleHttp\Client;

class Model
{

    private $app;
    public $tableProjects = 'fh_projects';
    private $tableEmployers = 'fh_employers';
    private $tableSkills = 'fh_skills';
    private $tableProjectSkills = 'fh_projects_skills';
    private $tableProjectRates = 'fh_rate';
    private $tableProjectStats = 'fh_stats';
    // при необходимости названия таблиц легко вынести в конфиг

    /**
     * Model constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function requestProjects($page = 0)
    {
        $client = new Client([
            'timeout' => 2.0,
        ]);

        if ($page) $pageURL = '?page[number]='.$page;
        else $pageURL = '';

        try {
            $response = $client->request('GET', 'https://api.freelancehunt.com/v2/projects'.$pageURL, [
                'headers' => [
                    'Authorization' => "Bearer 210f5f91b552ac78c1899548ae6802da47f560cf"
                ]
            ]);

            $statusCode = $response->getStatusCode();

            if (200 != $statusCode) {
                $this->app->log('Request projects fail');
                return null;
            }

            $json = (string)$response->getBody();
            $data = json_decode($json);

            if (empty($data->data) OR !is_array($data->data)) {
                $this->app->log('Request projects wrong data');
                return null;
            }
        } catch (\Throwable $t){
            $this->app->log('requestProjects fail: ' . $t->getMessage());
            return false;
        }


        return $data->data;
    }

    public function saveProjects(array $projects)
    {
        $db = $this->app->getDb();
        try {
            foreach ($projects as $project) {
                $query = "INSERT INTO $this->tableProjects 
                (id, name, description, employer_id, budget, currency, link) 
                VALUES (?, ?, ?, ?, ?, ?, ?)
                ON DUPLICATE KEY UPDATE
                name = ?, description = ?, employer_id = ?, budget = ?, currency = ?, link = ?
                ";
                /** @var mysqli_stmt $stmt */
                $stmt = $db->prepare($query);
                $stmt->bind_param
                (
                    'issiissssiiss',
                    $project->id,
                    $project->attributes->name,
                    $project->attributes->description,
                    $project->attributes->employer->id,
                    $project->attributes->budget->amount,
                    $project->attributes->budget->currency,
                    $project->links->self->web,
                    $project->attributes->name,
                    $project->attributes->description,
                    $project->attributes->employer->id,
                    $project->attributes->budget->amount,
                    $project->attributes->budget->currency,
                    $project->links->self->web
                );
                $stmt->execute();

                $query = "INSERT INTO $this->tableEmployers (id, login, first_name, last_name) VALUES (?, ?, ?, ?)
                ON DUPLICATE KEY UPDATE  login = ?, first_name = ?, last_name = ?";
                /** @var mysqli_stmt $stmt */
                $stmt = $db->prepare($query);
                $stmt->bind_param
                (
                    'issssss',
                    $project->attributes->employer->id,
                    $project->attributes->employer->login,
                    $project->attributes->employer->first_name,
                    $project->attributes->employer->last_name,
                    $project->attributes->employer->login,
                    $project->attributes->employer->first_name,
                    $project->attributes->employer->last_name
                );
                $stmt->execute();

                if (is_array($project->attributes->skills)) foreach ($project->attributes->skills as $skill) {
                    $query = "INSERT INTO $this->tableSkills (id, name) VALUES(?, ?)
                    ON DUPLICATE KEY UPDATE name  = ?";
                    $stmt = $db->prepare($query);
                    $stmt->bind_param
                    (
                        'iss',
                        $skill->id,
                        $skill->name,
                        $skill->name
                    );
                    $stmt->execute();

                    $db->query("DELETE FROM $this->tableProjectSkills WHERE project_id = $project->id");

                    $query = "INSERT INTO $this->tableProjectSkills (project_id, skill_id) VALUES(?, ?)";
                    $stmt = $db->prepare($query);
                    $stmt->bind_param
                    (
                        'ii',
                        $project->id,
                        $skill->id
                    );
                    $stmt->execute();
                }

            }
        } catch (\Throwable $t) {
            $this->app->log('saveProjects fail: ' . $t->getMessage());
            return false;
        }
        return true;
    }

    public function requestRates()
    {
        $client = new Client([
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5', [

        ]);

        $json = (string)$response->getBody();
        $data = json_decode($json);
        return $data;
    }

    public function saveRates($rates)
    {
        $db = $this->app->getDb();
        foreach ($rates as $rate) {
            $rates[$rate->ccy] = $rate->sale;
        }
        $query = "DELETE FROM $this->tableProjectRates";
        $db->query($query);
        $query = "INSERT INTO $this->tableProjectRates VALUES (" . $rates['RUR']
            . ", " . $rates['USD'] . ", " . $rates['EUR'] . ")";
        $db->query($query);
    }

    public function queryProjects($limit = '')
    {
        $message = 'queryProjects fail: ';
        $query = "SELECT pr.id, pr.name, pr.budget, pr.currency, emp.first_name, emp.last_name, emp.login FROM 
            $this->tableProjects pr LEFT JOIN $this->tableEmployers emp 
            ON pr.employer_id=emp.id 
            $limit";
        return $this->queryObjects($query, $message);

    }

    public function queryProjectsSkills($skills, $limit = '')
    {
        if (empty($skills) OR !is_array($skills)) return null;
        $message = 'queryProjects fail: ';
        $listSkills = implode(',', $skills);
        $query = "SELECT pr.id, pr.name, pr.budget, pr.currency, pr.link, emp.first_name, emp.last_name, emp.login, psk.skill_id
            FROM $this->tableProjects pr LEFT JOIN $this->tableEmployers emp ON pr.employer_id=emp.id 
            LEFT JOIN $this->tableProjectSkills psk ON pr.id = psk.project_id            
            WHERE psk.skill_id IN ($listSkills)
            $limit";
        return $this->queryObjects($query, $message);

    }

    public function queryProjectsSkillsCount($skills)
    {
        if (empty($skills) OR !is_array($skills)) return null;
        $message = 'queryProjects fail: ';
        $listSkills = implode(',', $skills);
        $query = "SELECT COUNT(pr.id) as count
            FROM $this->tableProjects pr LEFT JOIN $this->tableEmployers emp ON pr.employer_id=emp.id 
            LEFT JOIN $this->tableProjectSkills psk ON pr.id = psk.project_id            
            WHERE psk.skill_id IN ($listSkills)
            ";
        $data = $this->queryObjects($query, $message);
        if (empty($data) OR !is_array($data)) return 0;
        else return current($data)->count;

    }

    public function querySkillsStat()
    {

        $query = "SELECT sk.name, COUNT(sk.name) as projects FROM 
            $this->tableProjects pr, $this->tableSkills sk, $this->tableProjectSkills psk
            WHERE pr.id = psk.project_id AND sk.id = psk.skill_id
            GROUP BY sk.id
            ";
        $message = 'querySkills fail: ';
        return $this->queryObjects($query, $message);
    }

    public function queryRates()
    {
        $query = "SELECT * FROM 
            $this->tableProjectRates
            ";
        $message = 'queryRates fail: ';
        $data = $this->queryObjects($query, $message);
        if ($data) return current($data);
        else return [];
    }

    public function saveStats(array $stats)
    {
        $db = $this->app->getDb();
        $insert = implode(',', $stats);
        $query = "DELETE FROM $this->tableProjectStats";
        $db->query($query);
        $query = "INSERT INTO $this->tableProjectStats VALUES ($insert)";
        $db->query($query);
    }

    public function queryStats()
    {
        $query = "SELECT * FROM 
            $this->tableProjectStats
            ";
        $message = 'queryStats fail: ';
        $data = $this->queryObjects($query, $message);
        if ($data) return (array) current($data);
        else return [];
    }

    public function updateBudgetRates()
    {
        $db = $this->app->getDb();
        $rates = $this->queryRates();
        $query = "UPDATE $this->tableProjects SET budget_uah = IF(`currency` = 'RUB', budget * $rates->rate_rur , budget)";
        $db->query($query);
        $query = "UPDATE $this->tableProjects SET budget_group = IF(budget_uah < 500, 0 , budget_group)";
        $db->query($query);
        $query = "UPDATE $this->tableProjects SET budget_group = IF(budget_uah >= 500 AND budget_uah < 1000, 1 , budget_group)";
        $db->query($query);
        $query = "UPDATE $this->tableProjects SET budget_group = IF(budget_uah >= 1000 AND budget_uah < 5000, 2 , budget_group)";
        $db->query($query);
        $query = "UPDATE $this->tableProjects SET budget_group = IF(budget_uah >= 5000 AND budget_uah < 10000, 3 , budget_group)";
        $db->query($query);
        $query = "UPDATE $this->tableProjects SET budget_group = IF(budget_uah >= 10000, 4 , budget_group)";
        $db->query($query);

    }

    public function queryBudgetRatesCount()
    {
        $message = 'queryBudgetRatesCount fail';
        $query = "SELECT  COUNT(*) as count
            FROM $this->tableProjects 
            GROUP BY budget_group
            ";
        $data = $this->queryObjects($query, $message);
        if (empty($data) OR !is_array($data)) return 0;
        $result = [];
        foreach($data as $key=>$item) {
            $result[$key] = $item->count;
        };
        return $result;

    }

    public function queryObjects($query, $message)
    {
        $db = $this->app->getDb();
        try {
            $objects = [];
            if ($result = $db->query($query)) {
                while ($row = $result->fetch_object()) {
                    $objects[] = $row;
                }
            }
            return $objects;

        } catch (\Throwable $t) {
            $this->app->log($message . $t->getMessage());
            return [];
        }
    }

}