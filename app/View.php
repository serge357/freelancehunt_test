<?php

/**
 * Class View
 */
class View
{
    private $data = [];


    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * вывод обработанного шаблона в поток
     * @param string $name - имя шаблона
     */
    public function display($name)
    {
        $params = $this->data;
        $dir = __DIR__ . '/../templates';
        $templateFile =  $dir . '/' . $name;
        extract($params);
        include $templateFile;
    }

    /**
     * возврат обработанного шаблона
     * @param string $name - имя шаблона
     * @return string
     */
    public function prepare($name)
    {
        ob_start();
        $this->display($name);
        $text = ob_get_contents();
        ob_end_clean();
        return $text;
    }
}