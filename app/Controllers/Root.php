<?php

namespace Controllers;

class Root
{
    private $app;

    /**
     * Root constructor.
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    public function fetch($page)
    {
        $model = new \Model($this->app);
        $view =  new \View();

        $pageLimit = 10;

        $header = $view->prepare('header.php');
        $footer = $view->prepare('footer.php');

        if ($page > 1) {
            $start = ($page - 1) * $pageLimit;
            $limit = "LIMIT $start, $pageLimit";
        }
        else {
            $limit = "LIMIT $pageLimit";
        }
        $projects = $model->queryProjectsSkills([1, 86, 99], $limit);
        $view->set('projects', $projects);
        $skills = $model->querySkillsStat();
        $view->set('skills', $skills);

        $countProjects = $model->queryProjectsSkillsCount([1, 86, 99]);
        $countPages = (integer) ($countProjects / $pageLimit);
        if ($countProjects % $pageLimit > 0) $countPages++;
        $linkPage = '/';
        $view->set('countPages', $countPages);
        $view->set('linkPage', $linkPage);
        $view->set('currentPage', $page);
        $pagination = $view->prepare('paginate.php');
        $view->set('pagination', $pagination);

        $stats = $model->queryStats();

        array_shift($stats); // выкинул из диаграммы проекты с неуказанным бюджетом
        $listStats = implode(',', $stats);
        $view->set('stats', $listStats);

        $content = $view->prepare('root.php');

        return $header . $content .  $footer;

    }

}