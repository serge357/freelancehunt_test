<?php

namespace Controllers;

class Project
{
    private $app;

    /**
     * Root constructor.
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    public function fetch($page)
    {
        $model = new \Model($this->app);
        $view =  new \View();

        $header = $view->prepare('header.php');
        $footer = $view->prepare('footer.php');

        $content = 'Страница проекта';

        return $header . $content .  $footer;

    }

}