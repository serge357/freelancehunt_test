<?php

//namespace freelansehuntapp;

class App
{
    private $config;
    private $db;
    private $log = [];

    /**
     * App constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->db = new mysqli($config['host'],$config['user'], $config['password'],$config['db']);
        $this->db->set_charset('utf8');
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }


    /**
     * @return mixed[]
     */
    public static function requireConfig()
    {
        require_once 'config/config.php';
        /** @var mixed $config*/
        return $config;
    }

    /**
     * @return mixed
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @return array
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param mixed $var
     */
    public function log($var)
    {
        $this->log[] = $var;
    }


    /**
     * @param string $name
     * @param mixed $params
     * @return mixed
     */
    public function getController($name, $params = null)
    {
        $fullName = 'Controllers\\'.$name;
        $controller = new $fullName($this);
        if ($params) $controller->params = $params;
        return $controller;
    }

    /**
     * @param string $url
     * @return string
     */
    public function getRoute($url)
    {
        $routes = ['/' => 'Root', '/project' => 'Project'];
        return $routes[$url];
    }

    /**
     * @return Controller
     */
    public function router()
    {
        $uri = $_SERVER['REQUEST_URI'];
        $controllerName = $this->getRoute($uri);
        $controller = $this->getController($controllerName);
        return $controller;
    }

}