<?php

chdir(dirname(dirname(__FILE__)."/../.."));
require_once ('vendor/autoload.php');

$config = App::requireConfig();
$limitPages = $config['limitPages'];
$app = new App($config);
$model = new Model($app);

$saved = 0;

for ($i=0; $i<$limitPages; $i++) {
    $data = $model->requestProjects($i);
    if (is_array($data) AND count($data) > 0) {
        $save = $model->saveProjects($data);
        $saved += count($data);

    } else {
        break;
    }
}

// Для тестового задания нет необходимости скачивать базу целиком
// поэтому скачиваем пока АПИ отдает данные

var_export($app->getLog());
echo "\nLoaded: " . $saved . ' projects from ' . $i . "pages\n";

die();


