<?php
chdir(dirname(dirname(__FILE__)."/../.."));
require_once ('vendor/autoload.php');

$config = App::requireConfig();
$app = new App($config);
$model = new Model($app);
$rates = $model->requestRates();
if (!is_array($rates)) {
    echo 'Request Rates fail';
    die();
}

$model->saveRates($rates);

var_export($rates);
$model->updateBudgetRates();

$stats = $model->queryBudgetRatesCount();
$model->saveStats($stats);
var_export($stats);

var_export($app->getLog());

die();


