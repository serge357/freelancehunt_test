CREATE TABLE `fh_projects` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL DEFAULT NULL,
  `description` VARCHAR(50) NULL DEFAULT NULL,
  `employer_id` INT(10) UNSIGNED NULL DEFAULT NULL,
  `budget` INT(10) UNSIGNED NULL DEFAULT NULL,
  `currency` VARCHAR(4) NULL DEFAULT NULL,
  `link` VARCHAR(128) NOT NULL,
  `budget_uah` FLOAT NULL DEFAULT NULL,
  `budget_group` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `employer_id` (`employer_id`),
  INDEX `budget_group` (`budget_group`)
)
  COLLATE='utf8_general_ci'
;

CREATE TABLE `fh_employers` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) NULL DEFAULT NULL,
  `last_name` VARCHAR(50) NULL DEFAULT NULL,
  `login` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_general_ci'
;


CREATE TABLE `fh_skills` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE='utf8_general_ci'
;

CREATE TABLE `fn_projects_skills` (
  `project_id` INT(11) NULL DEFAULT NULL,
  `skill_id` INT(11) NULL DEFAULT NULL,
  UNIQUE INDEX `rels` (`project_id`, `skill_id`)
)
  COLLATE='utf8_general_ci'
;

CREATE TABLE `fh_rate` (
  `rate_rur` FLOAT NULL DEFAULT NULL,
  `rate_usd` FLOAT NULL DEFAULT NULL,
  `rate_eur` FLOAT NULL DEFAULT NULL
)
  COLLATE='utf8_general_ci'
;

CREATE TABLE `fh_stats` (
  `st0` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `st1` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `st2` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `st3` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `st4` INT(11) UNSIGNED NOT NULL DEFAULT '0'
)
  COLLATE='utf8_general_ci'
;
