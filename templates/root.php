<div class="album py-5 bg-light">
    <div class="container">
        <?php
        /** @var string $pagination */
        echo $pagination;
        ?>
        <hr>
        <h2 class="jumbotron-heading">Таблица открытых проектов в категориях Веб-программирование, PHP и Базы данных</h2>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Название проекта</th>
                <th scope="col">Бюджет</th>
                <th scope="col">Имя заказчика</th>
                <th scope="col">Логин заказчика</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            /** @var object[] $projects */
            foreach ($projects as $project) {
                $i++;
            ?>
            <tr>
                <th scope="row"><?= $i ?></th>
                <td><a href="<?= $project->link ?>"><?= $project->name ?></a></td>
                <td><?= $project->budget . ' ' . $project->currency ?></td>
                <td><?= $project->first_name . ' ' . $project->last_name ?></td>
                <td><?= $project->login ?></td>
            </tr>
            <?php } ?>

            </tbody>
        </table>

        <hr>
        <h2 class="jumbotron-heading">Таблица со статистикой всех открытых проектов по навыкам</h2>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Навык </th>
                <th scope="col">количество открытых проектов</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            /** @var object[] $skills */
            foreach ($skills as $skill) {
            $i++;
            ?>
            <tr>
                <th scope="row"><?= $i ?></th>
                <td><?= $skill->name ?></td>
                <td><?= $skill->projects ?></td>

            </tr>
            <?php } ?>

            </tbody>
        </table>

    <h2 class="jumbotron-heading">График с распределением всех проектов по бюджету</h2>
    <div class="album py-5 bg-light">
        <canvas id="myChart" width="990" height="495" style="display: block; width: 330px; height: 165px;"></canvas>
    </div>
    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['до 500 грн', '500-1000 грн', '1000-5000 грн', '5000-10000 грн', 'более 10000 грн'],
                datasets: [{
                    label: '# of Votes',
                    data: [<?= $stats ?>],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {

            }
        });
    </script>
    </div>
</div>