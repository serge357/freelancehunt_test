<?php

/** @var int $countPages */
/** @var string $linkPage */
/** @var string $currentPage */

$items = [];
for ($i = 1; $i < $countPages + 1; $i++) {
    if ($i == $currentPage OR ($currentPage == 0 AND $i== 1)) {
        $items[] = $i;
    } elseif ($i == 0 OR $i == 1) {
        $items[] = '<a href="' . $linkPage . '">' . $i . '</a>';
    } else {
        $items[] = '<a href="' . $linkPage . '?page=' . $i . '">' . $i . '</a>';
    }
}
echo implode( ' | ', $items);