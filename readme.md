**Инструкция по развертыванию**

Требования: php 7.*, mysql, apache

- Скачать репозиторий
- Создать файл app/config/config.php (по образцу app/config/config.example.php)
- Обновить composer
- Создать таблицы скриптом init_data/init.sql
- Загрузить базу проектов скриптом scripts/load_base.php
- Загрузить курсы валют и посчитать бюджетные группы скриптом scripts/load_rate.php
- После этого будет работать (страница в корне проекта)

