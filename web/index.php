<?php

require_once ('../vendor/autoload.php');

$config = App::requireConfig();
$app = new App($config);

$controllerName = $app->getRoute($_SERVER['REDIRECT_URL'] ?? '/');
$controller = $app->getController($controllerName);
$page = $_GET['page'] ?? 0;
$html = $controller->fetch($page);

echo $html;
die();

