<?php
/**
 * Created by PhpStorm.
 * UserOps: Sergey Pavlov
 * Date: 01.11.2017
 * Time: 11:37
 */

require_once ('../vendor/autoload.php');
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class TestRequests extends TestCase
{

    public function testSelf()
    {
        echo "работает";
    }

    public function testRequestProjects()
    {
        $client = new Client([
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', 'https://api.freelancehunt.com/v2/projects?page[number]=2', [
            'headers' => [
                'Authorization'     => "Bearer 210f5f91b552ac78c1899548ae6802da47f560cf"
            ]
        ]);

        $statusCode = $response->getStatusCode();
        $this->assertEquals(200,$statusCode);
        $json = (string)$response->getBody();
        $data = json_decode($json);
        $this->assertTrue(is_object($data));

    }
    public function testRequestRates()
    {
        $client = new Client([
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5', [

        ]);

        $statusCode = $response->getStatusCode();
        $this->assertEquals(200,$statusCode);
        $json = (string)$response->getBody();
        $data = json_decode($json);
        $this->assertTrue(is_array($data) && is_object(current($data)));

    }

    public function testRequestSkills()
    {
        $client = new Client([
            'timeout'  => 2.0,
        ]);

        $response = $client->request('GET', 'https://api.freelancehunt.com/v2/skills', [
            'headers' => [
                'Authorization'     => "Bearer 210f5f91b552ac78c1899548ae6802da47f560cf"
            ]
        ]);

        $statusCode = $response->getStatusCode();
        $this->assertEquals(200,$statusCode);
        $json = (string)$response->getBody();
        $data = json_decode($json);
        var_export($data);
        $this->assertTrue(is_object($data));

    }


}
