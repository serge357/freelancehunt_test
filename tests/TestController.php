<?php
/**
 * Created by PhpStorm.
 * UserOps: Sergey Pavlov
 * Date: 01.11.2017
 * Time: 11:37
 */

require_once ('../vendor/autoload.php');
use PHPUnit\Framework\TestCase;


class TestController extends TestCase
{

    public function testFetch()
    {
        $config = App::requireConfig();
        $app = new App($config);
        $controller = $app->getController('Root');
        $this->assertTrue(class_exists('Controllers\Root'));
        $this->assertTrue(is_object($controller));
        $text = $controller->fetch(2);
        $this->assertTrue(strlen($text) > 0);

    }


}
