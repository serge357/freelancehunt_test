<?php
/**
 * Created by PhpStorm.
 * UserOps: Sergey Pavlov
 * Date: 01.11.2017
 * Time: 11:37
 */

require_once ('../vendor/autoload.php');
use PHPUnit\Framework\TestCase;


class TestModel extends TestCase
{

    public function testRequestProjects()
    {
        $config = App::requireConfig();
        $app = new App($config);
        $this->assertTrue(is_object($app));
        $this->assertTrue(is_array($app->getConfig()));
        $model = new Model($app);
        $data = $model->requestProjects();
        $this->assertTrue(is_array($data));
    }

    public function testSaveProjects()
    {
        $config = App::requireConfig();
        $app = new App($config);
        $model = new Model($app);
        $data = $model->requestProjects();
        $save = $model->saveProjects($data);
        $this->assertTrue($save);
    }

    public function testQueryProjects()
    {
        $config = App::requireConfig();
        $app = new App($config);
        $model = new Model($app);
        $data = $model->queryProjects();

        $this->assertTrue(!empty($data));
    }

    public function testQueryProjectsSkills()
    {
        $config = App::requireConfig();
        $app = new App($config);
        $model = new Model($app);
        $data = $model->queryProjectsSkills([1, 86, 99]);

        $this->assertTrue(!empty($data));
    }

    public function testQueryProjectsSkillsCount()
    {
        $config = App::requireConfig();
        $app = new App($config);
        $model = new Model($app);
        $data = $model->queryProjectsSkillsCount([1, 86, 99]);

        $this->assertTrue(is_numeric($data));
    }

    public function testQuerySkillsStat()
    {
        $config = App::requireConfig();
        $app = new App($config);
        $model = new Model($app);
        $data = $model->querySkillsStat();

        $this->assertTrue(!empty($data));
    }

    public function testUpdateBudgetRates()
    {
        $config = App::requireConfig();
        $app = new App($config);
        $model = new Model($app);
        $model->updateBudgetRates();

        $this->assertTrue(!empty($data));
    }

    public function testQueryBudgetRatesCount()
    {
        $config = App::requireConfig();
        $app = new App($config);
        $model = new Model($app);
        $data = $model->queryBudgetRatesCount();
        $sum = array_sum($data);
        $query = "SELECT count(*) as c FROM $model->tableProjects";
        $count = $model->queryObjects($query, '');

        $this->assertTrue($sum == $count[0]->c);
    }

}
